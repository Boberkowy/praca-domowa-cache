import java.util.ArrayList;
import java.util.Map;
public class Cache{
	
	private Cache(){
	}
	
	private Map<String, ArrayList<systemEnumeration>>items;

	private static Cache cache;
	private static Object token = new Object();
	
	public static Cache getInstance()
	{
		if(cache == null){
			synchronized(token)
			{
				if(cache==null)
					cache = new Cache();
			}
		}
		return cache;
	}
	
	public synchronized void put(String id, ArrayList<systemEnumeration> list){
		items.put(id, list);
	}
	
	public synchronized void refresh(){
		this.clear();
		Update updater = new Update();
		updater.update();
	}
	
	
	
	public synchronized void clear(){
		items.clear();
		
	}

		
}