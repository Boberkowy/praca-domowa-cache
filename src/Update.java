import java.util.ArrayList;


public class Update implements Runnable{
	

	boolean isRunning = true;
	@Override
	public synchronized void run() {
	
		{
		isRunning = true;
			try {
				System.out.println("Updating cache");
				update();
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		isRunning=false;
		System.out.println("End.");
		}
	}
	
	public void update(){
		ArrayList<systemEnumeration> voidvodeship = getID();
		
		Cache cache = Cache.getInstance();
		cache.clear();
		cache.put("voidvodeship", voidvodeship);
	}

	private ArrayList<systemEnumeration> getID(){

		ArrayList<systemEnumeration> list = new ArrayList<systemEnumeration>();
		String name = "voidvodeship";
		systemEnumeration voidvodeship = new systemEnumeration();
		voidvodeship.setId(1);
		voidvodeship.setEnumID(1);
		voidvodeship.setCode("POM");
		voidvodeship.setValue("Pomorskie");
		voidvodeship.setEnumerationName("Pomo");
		list.add(voidvodeship);
		
		return list;
	}
	

}