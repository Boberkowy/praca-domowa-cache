public class systemEnumeration {

	private int id;
	private int enumID;
	private String code;
	private String value;
	private String enumerationName;
	
	
	public int getId() {
		return id;
	}
	public int getEnumID() {
		return enumID;
	}
	public String getCode() {
		return code;
	}
	public String getValue() {
		return value;
	}
	public String getEnumerationName() {
		return enumerationName;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setEnumID(int enumID) {
		this.enumID = enumID;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public void setEnumerationName(String enumerationName) {
		this.enumerationName = enumerationName;
	}
}	

